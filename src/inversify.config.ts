import 'reflect-metadata'
import { interfaces, TYPE, controller } from 'inversify-express-utils';
import { Container } from 'inversify';
import TYPES from './types/types';


import { ManageClientController } from './controller/ClientController';
import { ManageClientServices, ManageClientServicesImpl } from './service/ClientServices'
import { ManageClientRepository, ManageClientRepositoryMongo } from './repository/ClientRepository'


import {ProjectController} from './controller/ProjectController';
import {ProjectService,ProjectServicesImpl} from './service/ProjectService';
import {ProjectRepository,ProjectRepositoryMongo} from './repository/ProjectRepository'
const container = new Container();

container.bind<interfaces.Controller>(TYPE.Controller).to(ManageClientController).whenTargetNamed('ManageClientController');
container.bind<ManageClientServices>(TYPES.ManageClientServices).to(ManageClientServicesImpl)
container.bind<ManageClientRepository>(TYPES.ManageClientNoSQLRepository).to(ManageClientRepositoryMongo)



container.bind<interfaces.Controller>(TYPE.Controller).to(ProjectController).whenTargetNamed('ProjectController');
container.bind<ProjectService>(TYPES.ProjectService).to(ProjectServicesImpl)
container.bind<ProjectRepository>(TYPES.ProjectNoSQLRepository).to(ProjectRepositoryMongo)


export default container;