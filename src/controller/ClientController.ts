import { BaseController } from "./BaseController";
import { injectable, inject } from "inversify";
import * as express from "express";
import colors = require("colors");
import {
  interfaces,
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut
} from "inversify-express-utils";
import { ManageClientServices } from "../service/ClientServices";

import {
  ManageClientDTO,
  ManageClientMongoDatabase
} from "../schema/ClientSchema";
import { Demo } from "../model/Client";

import TYPES from "../types/types";
var ObjectID = require("mongodb").ObjectID;

@controller("/client")
@injectable()
export class ManageClientController extends BaseController
  implements interfaces.Controller {
  private manageClientServices: ManageClientServices;

  constructor(
    @inject(TYPES.ManageClientServices)
    manageClientServices: ManageClientServices
  ) {
    super();
    this.manageClientServices = manageClientServices;
  }

  /* 1.  getting the all managed Clients */

  @httpGet("/get")
  private async getAllClients(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const demo = await this.manageClientServices
      .getAllClients()
      .catch(err => console.log(err));
    return this.renderJSON(req, res, { demo: demo });
  }

  /* 2.  adding of new Clients */

  @httpPost("/create")
  public async addNewClient(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const clientData = req.body;
    try {
      var createdDemo = await this.manageClientServices.addNewClient(
        clientData
      );
    } catch (error) {
      return this.renderError(req, res, error);
    }
    return this.renderJSON(req, res, { demo: createdDemo }, 200);
  }

  /* 3. delete the Client by their id */

  @httpDelete("/delete")
  private async deleteClient(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    var id = req.body.id;
    var foundData = await this.manageClientServices.findclientById({
      _id: ObjectID(id)
    });
    if (foundData != null) {
      const deletedDemo = await this.manageClientServices
        .deleteClient(id)
        .catch(err => console.log(err));
      return this.renderJSON(
        req,
        res,
        { response: "this data is deleted" },
        200
      );
    } else {
      return this.renderJSON(req, res, { error: "this data is not available" });
    }
  }

  /* 4. update the client by their id */

  @httpPost("/update")
  private async updateClient(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.body.id;
    var datas = req.body;

    var foundData = await this.manageClientServices.findclientById({
      _id: ObjectID(id)
    });
    if (foundData != null) {
      await this.manageClientServices
        .updateClientById(id, datas)
        .catch(err => console.log(err));
      return this.renderJSON(
        req,
        res,
        { response: "this data is updated" },
        200
      );
    } else {
      return this.renderJSON(req, res, { error: "this data is deleted" }, 200);
    }
  }

  /* 5. find the client by their id */
  @httpPost("/findById")
  private async findclientById(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.body.id;
    var foundData = await this.manageClientServices.findclientById({
      _id: ObjectID(id)
    });
    if (foundData != null) {
      return this.renderJSON(req, res, { response: foundData }, 200);
    } else {
      return this.renderJSON(req, res, { error: "this data is not available" });
    }
  }

  /* activate the account */
  @httpPost("/activate")
  private async activate(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    var datas = req.body;
    const activation = await this.manageClientServices.activateTheClient(datas);
    return this.renderJSON(req, res, { response: activation }, 200);
  }
}
