import { NextFunction, Request, Response, Router } from "express";
import { injectable } from 'inversify';


@injectable()
export abstract class BaseController {

  constructor() {
  }


  public renderJSON(req: Request, res: Response, options: Object, status:number = 200) {
    res.status(status)
    return res.json(options)
  }


  public renderError(req:Request, res:Response, errors:any){
    res.status(500)
    return res.json({error:errors.message})
  }

}
