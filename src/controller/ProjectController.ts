import { BaseController } from "./BaseController";
import { injectable, inject } from "inversify";
import * as express from "express";
import colors = require("colors");
const _ = require("lodash");
const XLSX = require("xlsx");
import {
  interfaces,
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut
} from "inversify-express-utils";
import { ProjectService, ProjectServicesImpl } from "../service/ProjectService";

import {
  ManageClientDTO,
  ManageClientMongoDatabase
} from "../schema/ClientSchema";
import { Demo } from "../model/Client";

import TYPES from "../types/types";
var ObjectID = require("mongodb").ObjectID;
var counter = 1;
var number = 200;

@controller("/project")
@injectable()
export class ProjectController extends BaseController
  implements interfaces.Controller {
  private projectService: ProjectService;

  constructor(
    @inject(TYPES.ProjectService)
    projectService: ProjectService
  ) {
    super();
    this.projectService = projectService;
  }

  @httpPost("/create")
  private async createProject(req: any, res: express.Response) {
    var body = req.body;
    let user_assignment = [],
      project_type = [];
    if (req.body.user_assignment) {
      let users = req.body.user_assignment.split(" ");
      users.forEach(u => {
        user_assignment.push(u);
      });
    }

    if (req.body.project_type) {
      let projectTypes = req.body.project_type.split(" ");
      projectTypes.forEach(p => {
        project_type.push(p);
      });
    }
    body["editCount"] = 1;
    body["project"] = req.files;
    body["user_assignment"] = user_assignment;
    body["project_type"] = project_type;
    const response = await this.projectService.createProject(body);
    return this.renderJSON(req, res, { response: response });
  }

  @httpGet("/get")
  private async getProject(req: express.Request, res: express.Response) {
    const response = await this.projectService.getProject();
    return this.renderJSON(req, res, { response: response });
  }

  @httpPost("/findById")
  private async findById(req: express.Request, res: express.Response) {
    const id = req.body;
    console.log(req.body);
    const result = await this.projectService.findById(id);
    return this.renderJSON(req, res, { response: result });
  }

  @httpDelete("/delete")
  private async delete(req: express.Request, res: express.Response) {
    const id = req.body.id;
    const result = await this.projectService.delete(id);
    return this.renderJSON(req, res, { response: result });
  }

  @httpPost("/updateStatus")
  private async updateStatus(req: express.Request, res: express.Response) {
    const result = await this.projectService.updateStatus(req.body);
    return this.renderJSON(req, res, { response: result });
  }

  @httpPost("/findByStatus")
  private async activeProject(req: express.Request, res: express.Response) {
    const status = req.body.status;
    console.log(status)
    const result = await this.projectService.findByStatus(status);
    return this.renderJSON(req, res, { response: result });
  }

  @httpPost("/update")
  private async updateProject(req: express.Request, res: express.Response) {
    let body = req.body;
    body["editCount"] = counter++;
    const result = await this.projectService.updateProject(req.body._id, body);
    return this.renderJSON(req, res, { response: result });
  }

  @httpGet("/autoChangeStatus")
  private async autoChangeStatus(req: express.Request, res: express.Response) {
    var result = await this.projectService.autoChangeStatus();
    // result.forEach(r => {
    //   console.log(r.project_end_date)
    // });
    return this.renderJSON(req, res, { response: result });
  }
}
