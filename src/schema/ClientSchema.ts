import { Core, Model, Instance, Collection, Index, Property, ObjectID } from 'iridium';
import { Entity, Column, PrimaryColumn, OneToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';

import CONFIG from '../config'


export interface ManageClientDTO {
    _id?: string;
    mongoId?: string;
    demo: string;
}

/**
* Iridium config
*/

@Collection('clients')
export class ManageClientMongoSchema extends Instance<ManageClientDTO, ManageClientMongoSchema> implements ManageClientDTO {
    @ObjectID
    public _id: string;

    @Property(String, true)
    public demo: string;

}

class ManageClientDatabase extends Core {
    public model = new Model<ManageClientDTO, ManageClientMongoSchema>(this, ManageClientMongoSchema);

}

export const ManageClientMongoDatabase = new ManageClientDatabase(CONFIG.mongo());




/*user of client */
export interface ClientUserDTO {
    _id?: string;
    mongoId?: string;
    demo: string;
}

/**
* Iridium config
*/

@Collection('clientUsers')
export class ClientUserMongoSchema extends Instance<ClientUserDTO, ClientUserMongoSchema> implements ClientUserDTO {
    @ObjectID
    public _id: string;

    @Property(String, true)
    public demo: string;

}

class ClientUserDatabase extends Core {
    public model = new Model<ClientUserDTO, ClientUserMongoSchema>(this, ClientUserMongoSchema);

}

export const ClientUserMongoDatabase = new ClientUserDatabase(CONFIG.mongo());


/*activation code*/
export interface ActivationDTO {
    _id?: string;
    mongoId?: string;
}

/**
* Iridium config
*/

@Collection('Activation')
export class ActivationMongoSchema extends Instance<ActivationDTO, ActivationMongoSchema> implements ActivationDTO {
    @ObjectID
    public _id: string;

    @Property(String, true)
    public demo: string;

}

class ActivationDatabase extends Core {
    public model = new Model<ActivationDTO, ActivationMongoSchema>(this, ActivationMongoSchema);

}

export const ActivationMongoDatabase = new ActivationDatabase(CONFIG.mongo());


/*credential code*/
export interface CredentialDTO {
    _id?: string;
    mongoId?: string;
}

/**
* Iridium config
*/

@Collection('Credential')
export class CredentialMongoSchema extends Instance<CredentialDTO, CredentialMongoSchema> implements CredentialDTO {
    @ObjectID
    public _id: string;

    @Property(String, true)
    public demo: string;

}

class CredentialDatabase extends Core {
    public model = new Model<CredentialDTO, CredentialMongoSchema>(this, CredentialMongoSchema);

}

export const CredentialMongoDatabase = new CredentialDatabase(CONFIG.mongo());






