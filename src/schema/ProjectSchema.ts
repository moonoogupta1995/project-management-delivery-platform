import {
  Core,
  Model,
  Instance,
  Collection,
  Index,
  Property,
  ObjectID
} from "iridium";
import {
  Entity,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn
} from "typeorm";

import CONFIG from "../config";

export interface ProjectDTO {
  _id?: string;
  mongoId?: string;
  demo: string;
}

/**
 * Iridium config
 */

@Collection("projects")
export class ProjectMongoSchema extends Instance<ProjectDTO, ProjectMongoSchema>
  implements ProjectDTO {
  @ObjectID
  public _id: string;
  @Property(String, true)
  public demo: string;
}

class ProjectDatabase extends Core {
  public model = new Model<ProjectDTO, ProjectMongoSchema>(
    this,
    ProjectMongoSchema
  );
}

export const ProjectMongoDatabase = new ProjectDatabase(CONFIG.mongo());
