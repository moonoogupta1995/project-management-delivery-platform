var nodemailer = require("nodemailer");
import { injectable, inject } from "inversify";

import { promises } from "dns";

export interface EmailServices {
  sendEmail(code, hashdata, client);
}

@injectable()
export class EmailServicesImpl implements EmailServices {
  public async sendEmail(code, hashdata, createdDTO) {
    //console.log("bvcv",createdDTO,code)
    createdDTO[0].user.forEach(u => {
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "ajmf1802@gmail.com",
          pass: "biztruss123"
        }
      });
      const mailOptions = {
        from: "ajmf1802@gmail.com",
        to: u.emailid,
        subject: "Activation",
        html:
          "<h1>Welcome to Project Management Delivery Platform</h1><br>" +
          "<p>Here is your credential for activate your account</p>" +
          "<br> Password:" +
          hashdata +
          "<br>" +
          "<br> hashedKey:" +
          code +
          "<br>" +
          "<br> email:" +
          u.emailid +
          "<br>" +
          "<br> "
          + ' If you have any difficulty activating your account, feel free to  email our support team at support@appsol.com <br> <br>'
          + 'Support Team<br>'
          + 'Appsol Support Team'
  
      };

      transporter.sendMail(mailOptions, function(err, info) {
        if (err) console.log(err);
      });
    });
    return createdDTO;
  }
}
