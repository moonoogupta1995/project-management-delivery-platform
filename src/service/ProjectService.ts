import { injectable, inject } from "inversify";
var nodemailer = require("nodemailer");
import { Bcrypt } from "../util/Bcrypt";

import { Demo } from "../model/Client";

import {
  ProjectRepository,
  ProjectRepositoryMongo
} from "../repository/ProjectRepository";

import TYPES from "../types/types";

import { ProjectDTO } from "../schema/ProjectSchema";
import { EmailServicesImpl, EmailServices } from "./EmailService";
import { promises } from "dns";

export interface ProjectService {
  createProject(body);
  getProject();
  findById(id);
  delete(id);
  findByStatus(id);
  updateStatus(status);
  updateProject(id, data);
  autoChangeStatus();
}

@injectable()
export class ProjectServicesImpl implements ProjectService {
  @inject(TYPES.ProjectNoSQLRepository)
  private ProjectRepositoryMongo: ProjectRepository;
  private emailService = new EmailServicesImpl();

  public async createProject(body) {
    return await this.ProjectRepositoryMongo.createProject(body);
  }

  public async getProject() {
    return await this.ProjectRepositoryMongo.getProject();
  }

  public async findById(id) {
    return await this.ProjectRepositoryMongo.findById(id);
  }

  public async delete(id) {
    return await this.ProjectRepositoryMongo.delete(id);
  }

  public async findByStatus(status) {
    return await this.ProjectRepositoryMongo.findByStatus(status);
  }

  public async updateStatus(status) {
    return await this.ProjectRepositoryMongo.updateStatus(status);
  }

  public async updateProject(id, data) {
    return await this.ProjectRepositoryMongo.updateProject(id, data);
  }

  public async autoChangeStatus(){
    const allProjects = await this.ProjectRepositoryMongo.getProject();
    return allProjects;
  }
}
