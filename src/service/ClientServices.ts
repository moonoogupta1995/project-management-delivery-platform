import { injectable, inject } from "inversify";
var nodemailer = require("nodemailer");
import { Bcrypt } from "../util/Bcrypt";

import { Demo } from "../model/Client";

import { ManageClientRepository } from "../repository/ClientRepository";

import TYPES from "../types/types";

import { ManageClientDTO } from "../schema/ClientSchema";
import { EmailServicesImpl, EmailServices } from "./EmailService";
import { promises } from "dns";

export interface ManageClientServices {
  getAllClients();
  addNewClient(data);
  deleteClient(id);
  findclientById(id);
  updateClientById(id, datas);
  activateTheClient(datas);
}

@injectable()
export class ManageClientServicesImpl implements ManageClientServices {
  @inject(TYPES.ManageClientNoSQLRepository)
  private ManageClientRepositoryMongo: ManageClientRepository;
  private emailService = new EmailServicesImpl();

  public async getAllClients() {
    const demosMongo = await this.ManageClientRepositoryMongo.findAll();
    return demosMongo;
  }

  public async addNewClient(demoData) {
    var createdDTO = await this.ManageClientRepositoryMongo.addNewClient(
      demoData
    );
    await this.generateActivation(createdDTO);
    return createdDTO;
  }

  async generateActivation(client) {
    // console.log("client in service", client[0].user);
    const hashData = this.generateRandomPassword();
    const bcrypt = new Bcrypt();
    let code = await bcrypt.hash(hashData);

    try {
      await this.emailService.sendEmail(code, hashData, client);
      client[0].user.forEach(async u => {
        // console.log(u._id)
        await this.ManageClientRepositoryMongo.addActivation({
          client_id: u._id,
          code,
          hashData,
          email: u.emailid
        });
      });
      return client;
    } catch (error) {
      return error;
    }
  }

  private generateRandomPassword() {
    return Math.random()
      .toString(36)
      .slice(-8);
  }

  public async deleteClient(demoId) {
    return await this.ManageClientRepositoryMongo.deleteClient(demoId);
  }

  public async findclientById(id) {
    return await this.ManageClientRepositoryMongo.findclientById(id);
  }

  public async updateClientById(id, datas) {
    return await this.ManageClientRepositoryMongo.updateClientById(id, datas);
  }

  public async activateTheClient(datas) {
    return await this.ManageClientRepositoryMongo.activateTheClient(datas);
  }
}
