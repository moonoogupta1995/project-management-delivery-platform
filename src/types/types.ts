const TYPES = {

    ManageClientServices: Symbol('ManageClientServices'),
    ManageClientNoSQLRepository: Symbol('ManageClientNoSQLRepository'),

    ProjectService:Symbol('ProjectService'),
    ProjectNoSQLRepository:Symbol('ProjectNoSQLRepository')


};

export default TYPES;
