import DSNParser = require('dsn-parser')
interface PGConfigOptions {
    name: string,
    host: string,
    port: number,
    username: string,
    password: string,
    database: any,
    autoSchemaSync: boolean
}

const Config = {
    mongo: function(){
        if(process.env.MONGO_URL) {
            let dsn = new DSNParser(process.env.MONGO_URL)
            console.log('Mongo connection ..', dsn.getParts())
            return {
                host: dsn.get('host'),
                port: dsn.get('port'),
                username: dsn.get('user'),
                password: dsn.get('password'),
                database: dsn.get('database'),
            }
        }else{
            return {database: 'Project_Management_Delivery' }
        }
    }
}

export default Config
