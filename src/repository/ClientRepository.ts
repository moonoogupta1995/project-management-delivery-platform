import { injectable } from "inversify";
import { Repository, Connection } from "typeorm";
import {
  ManageClientDTO,
  ManageClientMongoDatabase,
  ManageClientMongoSchema,
  ClientUserDTO,
  ClientUserMongoDatabase,
  ClientUserMongoSchema,
  ActivationDTO,
  ActivationMongoDatabase,
  ActivationMongoSchema,
  CredentialDTO,
  CredentialMongoDatabase,
  CredentialMongoSchema
} from "../schema/ClientSchema";
var ObjectID = require("mongodb").ObjectID;

export interface ManageClientRepository {
  findAll();
  addNewClient(data);
  deleteClient(id);
  findclientById(id);
  updateClientById(id, datas);
  addActivation(args);
  activateTheClient(datas);
}

@injectable()
export class ManageClientRepositoryMongo implements ManageClientRepository {
  public async findAll() {
    const DemoDTOs = await ManageClientMongoDatabase.connect().then(() =>
      ManageClientMongoDatabase.model.find()
    );
    return DemoDTOs.toArray();
  }

  public async addNewClient(demoData) {
    await ManageClientMongoDatabase.connect();
    await ClientUserMongoDatabase.connect();
    var saveClient = await ManageClientMongoDatabase.model.collection.save(
      demoData
    );
    var count = 0;
    const result = await ManageClientMongoDatabase.model.collection
      .find({})
      .limit(1)
      .sort({ _id: -1 })
      .toArray();
    result[0].user.forEach(u => {
      u["client_id"] = result[0]._id;
    });
    await ClientUserMongoDatabase.model.collection.insertMany(result[0].user);
    return result;
  }

  public async deleteClient(id) {
    await ManageClientMongoDatabase.connect();
    const deleteresult = await ManageClientMongoDatabase.model.remove({
      _id: id
    });
    return deleteresult;
  }

  public async findclientById(id) {
    await ManageClientMongoDatabase.connect();
    const findClient = await ManageClientMongoDatabase.model.collection.findOne(
      id
    );
    return findClient;
  }

  public async updateClientById(id, datas) {
    await ManageClientMongoDatabase.connect();
    await ClientUserMongoDatabase.connect();
    delete datas["id"];

    const update = await ManageClientMongoDatabase.model.collection.updateOne(
      { _id: ObjectID(id) },
      { $set: datas }
    );
    //await ClientUserMongoDatabase.model.collection.insertMany(datas.user);
    return update;
  }

  public async addActivation(args) {
    //console.log("args",args)
    const activationData = args;
    await ActivationMongoDatabase.connect();
    const activation = await ActivationMongoDatabase.model.collection.insertOne(
      activationData
    );
    return activation;
  }

  public async activateTheClient(datas) {
    await ActivationMongoDatabase.connect();
    await ClientUserMongoDatabase.connect();
    await CredentialMongoDatabase.connect();
    const activation = await ActivationMongoDatabase.model.collection.findOne(
      datas
    );
    if (activation != null) {
      const user = await ClientUserMongoDatabase.model.collection.findOne({
        _id: ObjectID(activation.client_id)
      });
      //console.log(user, activation.client_id);
      await CredentialMongoDatabase.model.collection.insertOne({
        client_id: user._id,
        Email: user.emailid,
        Password: Math.random()
          .toString(36)
          .slice(-8)
      });
      return activation;
    } else {
      return "can't be activate now";
    }
  }
}
