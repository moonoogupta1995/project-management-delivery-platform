import { injectable } from "inversify";
import { Repository, Connection } from "typeorm";
import {
  ProjectDTO,
  ProjectMongoDatabase,
  ProjectMongoSchema
} from "../schema/ProjectSchema";
var ObjectID = require("mongodb").ObjectID;

export interface ProjectRepository {
  createProject(data);
  getProject();
  findById(id);
  delete(id);
  findByStatus(status);
  updateStatus(status);
  updateProject(id, data);
}

@injectable()
export class ProjectRepositoryMongo implements ProjectRepository {
  public async createProject(data) {
    await ProjectMongoDatabase.connect();
    const project = await ProjectMongoDatabase.model.collection.insertOne(data);
    return project;
  }

  public async getProject() {
    await ProjectMongoDatabase.connect();
    const result = await ProjectMongoDatabase.model.collection
      .find({})
      .toArray();
    return result;
  }

  public async findById(data) {
    console.log(data);
    await ProjectMongoDatabase.connect();
    const foundData = await ProjectMongoDatabase.model.collection.findOne({
      _id: ObjectID(data._id)
    });
    return foundData;
  }

  /* delete */
  public async delete(id) {
    await ProjectMongoDatabase.connect();
    const result = await ProjectMongoDatabase.model.collection.remove({
      _id: ObjectID(id)
    });
    return result;
  }

  public async findByStatus(status) {
    await ProjectMongoDatabase.connect();

    const result = await ProjectMongoDatabase.model.collection.find({
      status: status
    });
    return result.toArray();
  }

  public async updateStatus(data) {
    console.log(data);
    await ProjectMongoDatabase.connect();
    const foundData = await ProjectMongoDatabase.model.collection.findOne({
      _id: ObjectID(data._id)
    });
    if (foundData != null) {
      const result = await ProjectMongoDatabase.model.collection.update(
        { _id: ObjectID(data["_id"]) },
        { $set: { Status: data.status } }
      );
      return result;
    }
  }

  public async updateProject(id, data) {
    await ProjectMongoDatabase.connect();
    const foundData = await ProjectMongoDatabase.model.collection.findOne({
      _id: ObjectID(id)
    });
    delete data["_id"];
    if (foundData != null) {
      const result = await ProjectMongoDatabase.model.collection.update(
        { _id: ObjectID(id) },
        { $set: data }
      );
      return result;
    }
  }
}
