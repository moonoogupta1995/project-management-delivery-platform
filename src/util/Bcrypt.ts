import { genSaltSync, hashSync, compareSync } from "bcrypt-nodejs";
const SALT_ROUNDS = 8
export class Bcrypt{
    hash(args): string{
        const salt = genSaltSync(SALT_ROUNDS)
        return hashSync(args, salt)
    }
    compare(arg1, arg2): boolean{
        return compareSync(arg1, arg2)
    }
}
